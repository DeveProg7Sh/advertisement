<?php 

return [
    "menu"=>[
        "home"=>"Home",
        "about"=>"About",
        "services"=>"Services",
        "portfolio"=>"Portfolio",
        "products"=>"Products",
        "team"=>"Team",
        "contact"=>"Contact",
        "homepage"=>"Home Page"
    ],
    "home"=>[
        "caption"=>"Home",
        "text"=>"Text",
        "button"=>"Learn More",
        "goal"=>"We have Quality above all"
    ],
    "about"=>[
        "caption_h2"=>"About",
        "caption_p"=>"WHO WE ARE",
        "text"=>"Text",
        "call_action_caption"=>"Call Action Caption",
        "call_action_text"=>"Call Action Text",
        "call_action_button"=>"Call Action Button"
    ],
    "features"=>[
        "section1"=>"Information",
        "section2"=>"Additional Note",
        "section3"=>"Payment terms",
        "section4"=>"Delivery terms",
    ],
    "services"=>[
        "caption_h2"=>"Services",
        "caption_p"=>"WHAT WE DO OFFER",
        "hajm"=>"Volume: ",
        "chakana"=>"Retail price: ",
        "ulgurji"=>"Wholesale price: ",
        "ulgurji_kun"=>"Wholesale day: "
    ],
    "portfolio"=>[
        "caption_h2"=>"Portfolio",
        "caption_p"=>"WHAT WE'VE DONE"
    ],
    "testimonials"=>[
        "caption_h2"=>"Testimonials",
        "caption_p"=>"What they are saying about us",
        "person1_name"=>"1asdfadfadasfasasafsd",
        "person1_text"=>"1qwerqwerqwerqwerqwerq",
        "person2_name"=>"2asdfadfadasfasasafsd",
        "person2_text"=>"2qwerqwerqwerqwerqwerq",
        "person3_name"=>"3asdfadfadasfasasafsd",
        "person3_text"=>"3qwerqwerqwerqwerqwerq",
        "person4_name"=>"4asdfadfadasfasasafsd",
        "person4_text"=>"4qwerqwerqwerqwerqwerq",
        "person5_name"=>"5asdfadfadasfasasafsd",
        "person5_text"=>"5qwerqwerqwerqwerqwerq",
    ],
    "products"=>[
        "caption_h2"=>"Products",
        "caption_p"=>"Our Products",
        "nomi"=>"Name: ",
        "navi"=>"Grade: ",
        "hajm"=>"Volume: ",
        "gost"=>"GOST: ",
        "chet"=>"For Export: ",
        "chakana"=>"Retail price: ",
        "ulgurji"=>"Wholesale price: ",
        "ulgurji_kg"=>"Wholesale weight: ",
        "button"=>"More...",
        "uzb"=>"in Uzbekistan"
    ],
    "faq"=>[
        "caption_h2"=>"F.A.Q",
        "caption_p"=>"Frequently Asked Questions",
        "question1"=>"1qwerqwerqwerqwerqwerqwerqwer",
        "answer1"=>"1asdfasdfasdfasdfasdfasdfasdfsaasdf",
        "question2"=>"2qwerqwerqwerqwerqwerqwerqwer",
        "answer2"=>"2asdfasdfasdfasdfasdfasdfasdfsaasdf",
        "question3"=>"3qwerqwerqwerqwerqwerqwerqwer",
        "answer3"=>"3asdfasdfasdfasdfasdfasdfasdfsaasdf",
        "question4"=>"4qwerqwerqwerqwerqwerqwerqwer",
        "answer4"=>"4asdfasdfasdfasdfasdfasdfasdfsaasdf",

    ],
    "team"=>[
        "caption_h2"=>"Team",
        "caption_p"=>"Our Hardworking Team"
    ],
    "contact"=>[
        "caption_h2"=>"Contact",
        "caption_p"=>"Contact Us",
        "location_c"=>"Location",
        "location"=>"asdfqwerasdfqwerasdfqwerasdfqwerasd",
        "call"=>"Call",
        "name"=>"Your Name",
        "tel"=>"Your Phone Number",
        "message"=>"Message",
        "button"=>"Send Message"
    ],
    "footer"=>[
        "text"=>"qwerqwerqwerqwerqwerqwerqwer",
        "copyright"=>"Copyright",
        "copyright_2"=>"All Rights Reserved",
        "design"=>"Designed by "
    ]
];

?>