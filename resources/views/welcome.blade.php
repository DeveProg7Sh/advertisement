<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Master Mix</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <!-- <link href="{{asset('assets/img/favicon.png')}}" rel="icon">
    <link href="{{asset('assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon"> -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/icofont/icofont.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/line-awesome/css/line-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/venobox/venobox.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/owl.carousel/./assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/aos/aos.css')}}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

    <!-- =======================================================
    * Template Name: Selecao - v2.2.0
    * Template URL: https://bootstrapmade.com/selecao-bootstrap-template/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
    </head>
    <body>

        <!-- ======= Header ======= -->
        <header id="header" class="fixed-top d-flex align-items-center  header-transparent ">
            <div class="container d-flex align-items-center">

            <div class="logo mr-auto">
                <h1 class="text-light"><a href="#hero">Master Mix</a></h1>
                <!-- Uncomment below if you prefer to use an image logo -->
                <!-- <a href="index.html"><img src="{{asset('assets/img/logo.png')}}" alt="" class="img-fluid"></a>-->
            </div>

            <nav class="nav-menu d-none d-lg-block">
                <ul>
                <li class="active"><a href="#hero">{{ __('message.menu.home') }}</a></li>
                <li><a href="#about">{{ __('message.menu.about') }}</a></li>
                <li><a href="#services">{{ __('message.menu.services') }}</a></li>
                <li><a href="#pricing">{{ __('message.menu.products') }}</a></li>
                <li><a href="#team">{{ __('message.menu.team') }}</a></li>
                <li><a href="#contact">{{ __('message.menu.contact') }}</a></li>
                <li class="drop-down"><a href="#">
                @if(App::getLocale() == 'uz')
                    Uz
                @elseif(App::getLocale() == 'ru')
                    Ru
                @else
                    Eng
                @endif
                </a>
                    <ul>
                    <li><a href="/lang/uz">Uz</a></li>
                    <li><a href="/lang/ru">Ru</a></li>
                    <li><a href="/lang/en">Eng</a></li>
                    </ul>
                </li>
                </ul>
            </nav><!-- .nav-menu -->

            </div>
        </header><!-- End Header -->

        <!-- ======= Hero Section ======= -->
        <section id="hero" class="d-flex flex-column justify-content-end align-items-center">
            <div id="heroCarousel" class="container carousel carousel-fade" data-ride="carousel">

                <!-- Slide 1 -->
                <div class="carousel-item active">
                    <div class="carousel-container">
                    <h2 class="animate__animated animate__fadeInDown">Welcome to <span>Selecao</span></h2>
                    <p class="animate__animated fanimate__adeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                    <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read More</a>
                    </div>
                </div>

                <!-- Slide 2 -->
                <div class="carousel-item">
                    <div class="carousel-container">
                    <h2 class="animate__animated animate__fadeInDown">Lorem Ipsum Dolor</h2>
                    <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                    <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read More</a>
                    </div>
                </div>

                <!-- Slide 3 -->
                <div class="carousel-item">
                    <div class="carousel-container">
                    <h2 class="animate__animated animate__fadeInDown">Sequi ea ut et est quaerat</h2>
                    <p class="animate__animated animate__fadeInUp">Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.</p>
                    <a href="#about" class="btn-get-started animate__animated animate__fadeInUp scrollto">Read More</a>
                    </div>
                </div>

                <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon bx bx-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>

                <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon bx bx-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>

            <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28 " preserveAspectRatio="none">
            <defs>
                <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
            </defs>
            <g class="wave1">
                <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
            </g>
            <g class="wave2">
                <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
            </g>
            <g class="wave3">
                <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
            </g>
            </svg>

        </section><!-- End Hero -->

        <main id="main">

            <!-- ======= About Section ======= -->
            <section id="about" class="about">
            <div class="container">

                <div class="section-title" data-aos="zoom-out">
                <h2>{{ __('message.about.caption_h2') }}</h2>
                <p>{{ __('message.about.caption_p') }}</p>
                </div>

                <div class="row content" data-aos="fade-up">
                <div class="col-lg-6">
                    <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
                    magna aliqua.
                    </p>
                    <ul>
                    <li><i class="ri-check-double-line"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat</li>
                    <li><i class="ri-check-double-line"></i> Duis aute irure dolor in reprehenderit in voluptate velit</li>
                    <li><i class="ri-check-double-line"></i> Ullamco laboris nisi ut aliquip ex ea commodo consequat</li>
                    </ul>
                </div>
                <div class="col-lg-6 pt-4 pt-lg-0">
                    <p>
                    Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                    velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                    culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                    <a href="#" class="btn-learn-more">Learn More</a>
                </div>
                </div>

            </div>
            </section><!-- End About Section -->

            <!-- ======= Cta Section ======= -->
            <section id="cta" class="cta">
            <div class="container">

                <div class="row" data-aos="zoom-out">
                <div class="col-lg-9 text-center text-lg-left">
                    <h3>{{ __('message.about.call_action_caption') }}</h3>
                    <p> {{ __('message.about.call_action_text') }}</p>
                </div>
                <div class="col-lg-3 cta-btn-container text-center">
                    <a class="cta-btn align-middle" href="#contact">{{ __('message.about.call_action_button') }}</a>
                </div>
                </div>

            </div>
            </section><!-- End Cta Section -->

            <!-- ======= Services Section ======= -->
            @if(count($services) > 0)
            <section id="services" class="services">
                <div class="container">

                    <div class="section-title" data-aos="zoom-out">
                    <h2>{{ __('message.services.caption_h2') }}</h2>
                    <p>{{ __('message.services.caption_p') }}</p>
                    </div>

                    <div class="row">
                    @foreach($services as $service)
                    <div class="col-lg-4 col-md-6">
                        <div class="icon-box" data-aos="zoom-in-left">
                        <div class="icon"><i class="las la-clock" style="color: #d6ff22;"></i></div>
                        <h4 class="title"><a href="/service/{{$service->id}}">{{$service->avtnomi}}</a></h4>
                        <p class="description">
                            <b>{{ __('message.services.hajm') }}</b> {{$service->hajm}}<br>
                            <b>{{ __('message.services.chakana') }}</b> {{$service->chakana}} so`m<br>
                            <b>{{ __('message.services.ulgurji') }}</b> {{$service->ulgurji}} so`m<br>
                            <b>{{ __('message.services.ulgurji_kun') }}</b> {{$service->ulgurji_vaqt}}<br>
                        </p>
                        </div>
                    </div>
                    @endforeach
                    </div>

                </div>
            </section><!-- End Services Section -->
            @endif
            

            <!-- ======= Testimonials Section ======= -->
            <!-- <section id="testimonials" class="testimonials">
            <div class="container">

                <div class="section-title" data-aos="zoom-out">
                <h2>{{ __('message.testimonials.caption_h2') }}</h2>
                <p>{{ __('message.testimonials.caption_p') }}</p>
                </div>

                <div class="owl-carousel testimonials-carousel" data-aos="fade-up">

                <div class="testimonial-item">
                    <p>
                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                    {{ __('message.testimonials.person1_text') }}
                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                    <img src="{{asset('assets/img/testimonials/testimonials-1.jpg')}}" class="testimonial-img" alt="">
                    <h3>{{ __('message.testimonials.person2_name') }}</h3>
                </div>

                <div class="testimonial-item">
                    <p>
                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                    {{ __('message.testimonials.person3_text') }}
                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                    <img src="{{asset('assets/img/testimonials/testimonials-2.jpg')}}" class="testimonial-img" alt="">
                    <h3>{{ __('message.testimonials.person3_name') }}</h3>
                </div>

                <div class="testimonial-item">
                    <p>
                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                    {{ __('message.testimonials.person4_text') }}
                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                    <img src="{{asset('assets/img/testimonials/testimonials-3.jpg')}}" class="testimonial-img" alt="">
                    <h3>{{ __('message.testimonials.person4_name') }}</h3>
                </div>

                <div class="testimonial-item">
                    <p>
                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                    {{ __('message.testimonials.person5_text') }}
                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                    <img src="{{asset('assets/img/testimonials/testimonials-4.jpg')}}" class="testimonial-img" alt="">
                    <h3>{{ __('message.testimonials.person5_name') }}</h3>
                </div>

                <div class="testimonial-item">
                    <p>
                    <i class="bx bxs-quote-alt-left quote-icon-left"></i>
                    {{ __('message.testimonials.person5_text') }}
                    <i class="bx bxs-quote-alt-right quote-icon-right"></i>
                    </p>
                    <img src="{{asset('assets/img/testimonials/testimonials-5.jpg')}}" class="testimonial-img" alt="">
                    <h3>{{ __('message.testimonials.person5_name') }}</h3>
                </div>

                </div>

            </div>
            </section -->
            ><!-- End Testimonials Section -->

            <!-- ======= Pricing Section ======= -->
            <section id="pricing" class="pricing">
            <div class="container">

                <div class="section-title" data-aos="zoom-out">
                <h2>{{ __('message.products.caption_h2') }}</h2>
                <p>{{ __('message.products.caption_p') }}</p>
                </div>

                <div class="row">
                @foreach($products as $product)
                    <div class="col-lg-3 col-md-6">
                        <div class="box" data-aos="zoom-in">
                        <h3>
                            @if(Session::get('applocale') == "ru" && $product->nomi_rus != null)
                                {{$product->nomi_rus}}
                            @elseif(Session::get('applocale') == "en" && $product->nomi_eng != null)
                                {{$product->nomi_eng}}
                            @else
                                {{$product->nomi_uzb}}
                            @endif
                        </h3>
                        <h4><sup>$</sup>{{$product->chet_valyuta}}<span> {{ __('message.products.chet') }}</span></h4>
                        <ul>
                            <li style="text-align: left;"><b>{{ __('message.products.navi') }}</b>{{$product->sort}}</li>
                            <li style="text-align: left;"><b>{{ __('message.products.hajm') }}</b>{{$product->hajm}}</li>
                            <li style="text-align: left;"><b>{{ __('message.products.gost') }}</b>{{$product->gost}}</li>
                            <li style="text-align: left;"><b>{{ __('message.products.chakana') }}</b>{{$product->chakana}} so`m ( {{ __('message.products.uzb') }} )</li>
                            <li style="text-align: left;"><b>{{ __('message.products.ulgurji') }}</b>{{$product->ulgurji}} so`m ( {{ __('message.products.uzb') }} )</li>
                            <li style="text-align: left;"><b>{{ __('message.products.ulgurji_kg') }}</b>{{$product->ulgurji_kg}}</li>
                        </ul>
                        <div class="btn-wrap">
                            <a href="/product/{{$product->id}}" class="btn-buy">{{ __('message.products.button') }}</a>
                        </div>
                        </div>
                    </div>
                @endforeach
                </div>

            </div>
            </section><!-- End Pricing Section -->

            <!-- ======= F.A.Q Section ======= -->
            <section id="faq" class="faq">
            <div class="container">

                <div class="section-title" data-aos="zoom-out">
                <h2>{{ __('message.faq.caption_h2') }}</h2>
                <p>Frequently Asked Questions</p>
                </div>

                <ul class="faq-list">

                <li data-aos="fade-up" data-aos-delay="100">
                    <a data-toggle="collapse" class="" href="#faq1">{{ __('message.faq.question1') }} <i class="icofont-simple-up"></i></a>
                    <div id="faq1" class="collapse show" data-parent=".faq-list">
                    <p>
                    {{ __('message.faq.answer1') }}
                    </p>
                    </div>
                </li>

                <li data-aos="fade-up" data-aos-delay="200">
                    <a data-toggle="collapse" href="#faq2" class="collapsed">{{ __('message.faq.question2') }} <i class="icofont-simple-up"></i></a>
                    <div id="faq2" class="collapse" data-parent=".faq-list">
                    <p>
                    {{ __('message.faq.answer2') }}
                    </p>
                    </div>
                </li>

                <li data-aos="fade-up" data-aos-delay="200">
                    <a data-toggle="collapse" href="#faq3" class="collapsed">{{ __('message.faq.question3') }} <i class="icofont-simple-up"></i></a>
                    <div id="faq3" class="collapse" data-parent=".faq-list">
                    <p>
                    {{ __('message.faq.answer3') }}
                    </p>
                    </div>
                </li>

                <li data-aos="fade-up" data-aos-delay="200">
                    <a data-toggle="collapse" href="#faq4" class="collapsed">{{ __('message.faq.question4') }} <i class="icofont-simple-up"></i></a>
                    <div id="faq4" class="collapse" data-parent=".faq-list">
                    <p>
                    {{ __('message.faq.answer4') }}
                    </p>
                    </div>
                </li>
                
                </ul>

            </div>
            </section><!-- End F.A.Q Section -->

            <!-- ======= Team Section ======= -->
            <section id="team" class="team">
            <div class="container">

                <div class="section-title" data-aos="zoom-out">
                <h2>{{ __('message.team.caption_h2') }}</h2>
                <p>{{ __('message.team.caption_p') }}</p>
                </div>

                <div class="row">
                @foreach($xodimlar as $xodim)
                    <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
                        <div class="member" data-aos="fade-up">
                        <div class="member-img">
                            <img src="{{$xodim->rasm}}" class="img-fluid" alt="">
                            <div class="social">
                            <a href="{{$xodim->whatsapp}}" target="_blank"><i class="icofont-whatsapp"></i></a>
                            <a href="{{$xodim->telegram}}" target="_blank"><i class="icofont-telegram"></i></a>
                            </div>
                        </div>
                        <div class="member-info">
                            <h4>{{$xodim->ismi}}</h4>
                            <span>{{$xodim->lavozimi}}</span>
                            <div style="text-align: center;">
                                <a style="font-size: 40px;" href="#team"><i class="icofont-email"></i></a><br>{{$xodim->epochta}}<br>
                                <a style="font-size: 40px;" href="#team"><i class="icofont-mobile-phone"></i></a><br>{{$xodim->telnomer}}
                            </div>
                        </div>
                        </div>
                    </div>
                @endforeach

                </div>

            </div>
            </section><!-- End Team Section -->

            <!-- ======= Contact Section ======= -->
            <section id="contact" class="contact">
            <div class="container">
            
                <div class="section-title" data-aos="zoom-out">
                <h2>{{ __('message.contact.caption_h2') }}</h2>
                <p>{{ __('message.contact.caption_p') }}</p>
                </div>
                <div>
                    
                <iframe style="border:0; width: 100%; height: 270px;" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d732.1015595816917!2d67.87407166877608!3d40.10702991277208!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e1!3m2!1sru!2s!4v1644925213437!5m2!1sru!2s" frameborder="0" allowfullscreen></iframe>
                </div>
                <div class="row mt-5">

                <div class="col-lg-4" data-aos="fade-right">
                    <div class="info">
                    <div class="address">
                        <i class="icofont-google-map"></i>
                        <h4>{{ __('message.contact.location_c') }}</h4>
                        <p>{{ __('message.contact.location') }}</p>
                    </div>

                    <div class="email">
                        <i class="icofont-envelope"></i>
                        <h4>Email:</h4>
                        <p>info@example.com</p>
                    </div>

                    <div class="phone">
                        <i class="icofont-phone"></i>
                        <h4>{{ __('message.contact.call') }}:</h4>
                        <p>+1 5589 55488 55s</p>
                    </div>

                    </div>

                </div>

                <div class="col-lg-8 mt-5 mt-lg-0" data-aos="fade-left">

                    <form action="/send_post" method="post" role="form" class="php-email-form">
                    @csrf
                    <div class="form-row">
                        <div class="col-md-6 form-group">
                        <input type="text" name="name" class="form-control" id="name" placeholder="{{__('message.contact.name')}}" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                        <div class="validate"></div>
                        </div>
                        <div class="col-md-6 form-group">
                        <input type="tel" class="form-control" name="tel" id="tel" data-rule="required" placeholder="{{__('message.contact.tel')}}" pattern="+[0-9]{3,12}" data-msg="Please enter a valid phone number" />
                        <div class="validate"></div>
                        </div>
                    </div>  
                    <div class="form-group">
                        <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="{{__('message.contact.message')}}"></textarea>
                        <div class="validate"></div>
                    </div>
                    <div class="mb-3">
                        <div class="loading">Loading</div>
                        <!-- <div class="error-message"></div> -->
                        <!-- <div class="sent-message">Your message has been sent. Thank you!</div> -->
                    </div>
                    <div class="text-center"><button type="submit">Send Message</button></div>
                    </form>

                </div>

                </div>

            </div>
            </section><!-- End Contact Section -->

        </main><!-- End #main -->

        <!-- ======= Footer ======= -->
        <footer id="footer">
            <div class="container">
            <h3>Master Mix</h3>
            <p>{{__('message.footer.text')}}</p>
            <div class="social-links">
                <a href="#" class="twitter"><i class="bx bxl-telegram"></i></a>
                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="https://mail.google.com/mail/u/0/#inbox?compose=DmwnWtMpdKkPNCDHrMjgSxLQNkHvNrtvBvsPTJpGGBGnKwFVsfGkzxxcSKXKvTTHWlFpjBMGSTZQ" class="google-plus" target="_blank"><i class="icofont-email"></i></a>
            </div>
            <div class="copyright">
            &copy; {{__('message.footer.copyright')}} <strong><span>Master Mix</span></strong>. {{__('message.footer.copyright_2')}}
            </div>
            <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/selecao-bootstrap-template/ -->
                {{__('message.footer.design')}} <a href="https://t.me/deveprog7sh" target="_blank">DeveProg7Sh</a>
            </div>
            </div>
        </footer><!-- End Footer -->

        <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
        <a href="#contact" class="back-to-contact"><i class="icofont-phone"></i></a>

        <!-- Vendor JS Files -->
        <script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('assets/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
        <script src="{{asset('assets/vendor/php-email-form/validate.js')}}"></script>
        <script src="{{asset('assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
        <script src="{{asset('assets/vendor/venobox/venobox.min.js')}}"></script>
        <script src="{{asset('assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
        <script src="{{asset('assets/vendor/aos/aos.js')}}"></script>

        <!-- Template Main JS File -->
        <script src="{{asset('assets/js/main.js')}}"></script>

    </body>
</html>
