<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Master Mix</title>
    <meta content="" name="descriptison">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <!-- <link href="{{asset('assets/img/favicon.png')}}" rel="icon">
    <link href="{{asset('assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon"> -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/icofont/icofont.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/line-awesome/css/line-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/venobox/venobox.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/owl.carousel/./assets/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/aos/aos.css')}}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

    <!-- =======================================================
    * Template Name: Selecao - v2.2.0
    * Template URL: https://bootstrapmade.com/selecao-bootstrap-template/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
    </head>
    <body>

        <!-- ======= Header ======= -->
        <header id="header" class="fixed-top d-flex align-items-center  header-transparent ">
            <div class="container d-flex align-items-center">

            <div class="logo mr-auto">
                <h1 class="text-light"><a href="/home">Master Mix</a></h1>
                <!-- Uncomment below if you prefer to use an image logo -->
                <!-- <a href="index.html"><img src="{{asset('assets/img/logo.png')}}" alt="" class="img-fluid"></a>-->
            </div>

            <nav class="nav-menu d-none d-lg-block">
                <ul>
                <li class="active"><a href="#hero">{{ __('message.menu.home') }}</a></li>
                <li><a href="#features">{{ __('message.menu.about') }}</a></li>
                <li><a href="#contact">{{ __('message.menu.contact') }}</a></li>
                <li><a href="/home">{{ __('message.menu.homepage') }}</a></li>
                <li class="drop-down"><a href="#">
                @if(App::getLocale() == 'uz')
                    Uz
                @elseif(App::getLocale() == 'ru')
                    Ru
                @else
                    Eng
                @endif
                </a>
                    <ul>
                    <li><a href="/lang/uz">Uz</a></li>
                    <li><a href="/lang/ru">Ru</a></li>
                    <li><a href="/lang/en">Eng</a></li>
                    </ul>
                </li>
                </ul>
            </nav><!-- .nav-menu -->

            </div>
        </header><!-- End Header -->

        <!-- ======= Hero Section ======= -->
        <section id="hero" class="d-flex flex-column justify-content-end align-items-center">
            <div id="heroCarousel" class="container carousel carousel-fade" data-ride="carousel">

                <!-- Slide 1 -->
                <div class="carousel-item active">
                    <div class="carousel-container">
                    <h2 class="animate__animated animate__fadeInDown">{{$main_product->avtnomi}}</span></h2>
                    <p class="animate__animated fanimate__adeInUp">{{__('message.home.goal')}}</p>
                    <a href="#features" class="btn-get-started animate__animated animate__fadeInUp scrollto">{{__('message.home.button')}}</a>
                    </div>
                </div>

            </div>

            <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 24 150 28 " preserveAspectRatio="none">
            <defs>
                <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
            </defs>
            <g class="wave1">
                <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
            </g>
            <g class="wave2">
                <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
            </g>
            <g class="wave3">
                <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
            </g>
            </svg>

        </section><!-- End Hero -->

        <main id="main">

            <!-- ======= Features Section ======= -->
            <section id="features" class="features">
                <div class="container">

                    <ul class="nav nav-tabs row d-flex">
                        <li class="nav-item col-4" data-aos="zoom-in">
                            <a class="nav-link active show" data-toggle="tab" href="#tab-1">
                            <i class="ri-gps-line"></i>
                            <h4 class="d-none d-lg-block">{{__('message.features.section1')}}</h4>
                            </a>
                        </li>
                        <li class="nav-item col-4" data-aos="zoom-in" data-aos-delay="100">
                            <a class="nav-link" data-toggle="tab" href="#tab-2">
                            <i class="ri-body-scan-line"></i>
                            <h4 class="d-none d-lg-block">{{__('message.features.section2')}}</h4>
                            </a>
                        </li>
                        <li class="nav-item col-4" data-aos="zoom-in" data-aos-delay="200">
                            <a class="nav-link" data-toggle="tab" href="#tab-3">
                            <i class="ri-sun-line"></i>
                            <h4 class="d-none d-lg-block">{{__('message.features.section3')}}</h4>
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content" data-aos="fade-up">
                    <div class="tab-pane active show" id="tab-1">
                        <div class="row">
                            <div class="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0">
                                <div style="border: 2px #ddd solid;">
                                    <div style="margin: 10px;"><b style="color: #ef6603;">{{ __('message.services.hajm') }}</b>{{$main_product->sort}}</div>
                                    <div style="margin: 10px;"><b style="color: #ef6603;">{{ __('message.services.chakana') }}</b>{{$main_product->chakana}} so`m ( {{ __('message.products.uzb') }} )</div>
                                    <div style="margin: 10px;"><b style="color: #ef6603;">{{ __('message.services.ulgurji') }}</b>{{$main_product->ulgurji}} so`m ( {{ __('message.products.uzb') }} )</div>
                                    <div style="margin: 10px;"><b style="color: #ef6603;">{{ __('message.services.ulgurji_kun') }}</b>{{$main_product->ulgurji_vaqt}}</div>
                                </div>
                            </div>
                        <div class="col-lg-6 order-1 order-lg-2 text-center">
                            <img src="{{asset('assets/img/features-1.png')}}" alt="" class="img-fluid">
                        </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-2">
                        <div class="row">
                            <div class="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0">
                                <div style="border: 2px #ddd solid;">
                                    @if(Session::get('applocale') == "ru" && $main_product->izoh_rus != null)
                                        {{$main_product->izoh_rus}}
                                    @elseif(Session::get('applocale') == "en" && $main_product->izoh_eng != null)
                                        {{$main_product->izoh_eng}}
                                    @else
                                        {{$main_product->izoh_uzb}}
                                    @endif
                                </div>  
                            </div>
                        <div class="col-lg-6 order-1 order-lg-2 text-center">
                            <img src="{{asset('assets/img/features-2.png')}}" alt="" class="img-fluid">
                        </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-3">
                        <div class="row">
                            <div class="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0">
                                <div style="border: 2px #ddd solid;">
                                    @if(Session::get('applocale') == "ru" && $main_product->tolov_rus != null)
                                        {{$main_product->tolov_rus}}
                                    @elseif(Session::get('applocale') == "en" && $main_product->tolov_eng != null)
                                        {{$main_product->tolov_eng}}
                                    @else
                                        {{$main_product->tolov_uzb}}
                                    @endif
                                </div>  
                            </div>
                        <div class="col-lg-6 order-1 order-lg-2 text-center">
                            <img src="{{asset('assets/img/features-3.png')}}" alt="" class="img-fluid">
                        </div>
                        </div>
                    </div>
                    </div>

                </div>
            </section><!-- End Features Section -->

            <!-- ======= Portfolio Section ======= -->
            <section id="portfolio" class="portfolio">
                <div class="container">

                    <div class="section-title" data-aos="zoom-out">
                    <h2>{{ __('message.portfolio.caption_h2') }}</h2>
                    <p>{{ __('message.portfolio.caption_p') }}</p>
                    </div>

                    

                    <div class="row portfolio-container" data-aos="fade-up">
                    @foreach($main_product->avtxizmatrasm as $rasm)
                        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
                            <div class="portfolio-img"><img src="{{$rasm->path}}" class="img-fluid" alt=""></div>
                            <div class="portfolio-info">
                            <h4>----</h4>
                            <p>-----</p>
                            <a href="{{$rasm->path}}" data-gall="portfolioGallery" class="venobox preview-link"><i class="bx bxs-bullseye"></i></a>
                            </div>
                        </div>
                    @endforeach
                    <!-- <div class="col-lg-4 col-md-6 portfolio-item filter-card">
                        <div class="portfolio-img"><img src="{{asset('assets/img/portfolio/portfolio-4.jpg')}}" class="img-fluid" alt=""></div>
                        <div class="portfolio-info">
                        <h4>Card 2</h4>
                        <p>Card</p>
                        <a href="{{asset('assets/img/portfolio/portfolio-4.jpg')}}" data-gall="portfolioGallery" class="venobox preview-link" title="Card 2"><i class="bx bx-plus"></i></a>
                        <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 portfolio-item filter-web">
                        <div class="portfolio-img"><img src="{{asset('assets/img/portfolio/portfolio-5.jpg')}}" class="img-fluid" alt=""></div>
                        <div class="portfolio-info">
                        <h4>Web 2</h4>
                        <p>Web</p>
                        <a href="{{asset('assets/img/portfolio/portfolio-5.jpg')}}" data-gall="portfolioGallery" class="venobox preview-link" title="Web 2"><i class="bx bx-plus"></i></a>
                        <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
                        </div>
                    </div> -->

                    </div>

                </div>
            </section><!-- End Portfolio Section -->

            <!-- ======= Cta Section ======= -->
            <section id="cta" class="cta">
                <div class="container">

                    <div class="row" data-aos="zoom-out">
                    <div class="col-lg-9 text-center text-lg-left">
                        <h3>{{ __('message.about.call_action_caption') }}</h3>
                        <p> {{ __('message.about.call_action_text') }}</p>
                    </div>
                    <div class="col-lg-3 cta-btn-container text-center">
                        <a class="cta-btn align-middle" href="#contact">{{ __('message.about.call_action_button') }}</a>
                    </div>
                    </div>

                </div>
            </section><!-- End Cta Section -->

            <!-- ======= Services Section ======= -->
            @if(count($services) > 0)
            <section id="services" class="services">
                <div class="container">

                    <div class="section-title" data-aos="zoom-out">
                    <h2>{{ __('message.services.caption_h2') }}</h2>
                    <p>{{ __('message.services.caption_p') }}</p>
                    </div>

                    <div class="row">
                    @foreach($services as $service)
                    <div class="col-lg-4 col-md-6">
                        <div class="icon-box" data-aos="zoom-in-left">
                        <div class="icon"><i class="las la-clock" style="color: #d6ff22;"></i></div>
                        <h4 class="title"><a href="/service/{{$service->id}}">{{$service->avtnomi}}</a></h4>
                        <p class="description">
                            <b>{{ __('message.services.hajm') }}</b> {{$service->hajm}}<br>
                            <b>{{ __('message.services.chakana') }}</b> {{$service->chakana}} so`m<br>
                            <b>{{ __('message.services.ulgurji') }}</b> {{$service->ulgurji}} so`m<br>
                            <b>{{ __('message.services.ulgurji_kun') }}</b> {{$service->ulgurji_vaqt}}<br>
                        </p>
                        </div>
                    </div>
                    @endforeach
                    </div>

                </div>
            </section><!-- End Services Section -->
            @endif

            <!-- ======= F.A.Q Section ======= -->
            <section id="faq" class="faq">
            <div class="container">

                <div class="section-title" data-aos="zoom-out">
                <h2>{{ __('message.faq.caption_h2') }}</h2>
                <p>Frequently Asked Questions</p>
                </div>

                <ul class="faq-list">

                <li data-aos="fade-up" data-aos-delay="100">
                    <a data-toggle="collapse" class="" href="#faq1">{{ __('message.faq.question1') }} <i class="icofont-simple-up"></i></a>
                    <div id="faq1" class="collapse show" data-parent=".faq-list">
                    <p>
                    {{ __('message.faq.answer1') }}
                    </p>
                    </div>
                </li>

                <li data-aos="fade-up" data-aos-delay="200">
                    <a data-toggle="collapse" href="#faq2" class="collapsed">{{ __('message.faq.question2') }} <i class="icofont-simple-up"></i></a>
                    <div id="faq2" class="collapse" data-parent=".faq-list">
                    <p>
                    {{ __('message.faq.answer2') }}
                    </p>
                    </div>
                </li>

                <li data-aos="fade-up" data-aos-delay="200">
                    <a data-toggle="collapse" href="#faq3" class="collapsed">{{ __('message.faq.question3') }} <i class="icofont-simple-up"></i></a>
                    <div id="faq3" class="collapse" data-parent=".faq-list">
                    <p>
                    {{ __('message.faq.answer3') }}
                    </p>
                    </div>
                </li>

                <li data-aos="fade-up" data-aos-delay="200">
                    <a data-toggle="collapse" href="#faq4" class="collapsed">{{ __('message.faq.question4') }} <i class="icofont-simple-up"></i></a>
                    <div id="faq4" class="collapse" data-parent=".faq-list">
                    <p>
                    {{ __('message.faq.answer4') }}
                    </p>
                    </div>
                </li>
                
                </ul>

            </div>
            </section><!-- End F.A.Q Section -->

            

            <!-- ======= Contact Section ======= -->
            <section id="contact" class="contact">
                <div class="container">
                
                    <div class="section-title" data-aos="zoom-out">
                    <h2>{{ __('message.contact.caption_h2') }}</h2>
                    <p>{{ __('message.contact.caption_p') }}</p>
                    </div>
                    <div>
                        
                    <iframe style="border:0; width: 100%; height: 270px;" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d732.1015595816917!2d67.87407166877608!3d40.10702991277208!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e1!3m2!1sru!2s!4v1644925213437!5m2!1sru!2s" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="row mt-5">

                    <div class="col-lg-4" data-aos="fade-right">
                        <div class="info">
                        <div class="address">
                            <i class="icofont-google-map"></i>
                            <h4>{{ __('message.contact.location_c') }}</h4>
                            <p>{{ __('message.contact.location') }}</p>
                        </div>

                        <div class="email">
                            <i class="icofont-envelope"></i>
                            <h4>Email:</h4>
                            <p>info@example.com</p>
                        </div>

                        <div class="phone">
                            <i class="icofont-phone"></i>
                            <h4>{{ __('message.contact.call') }}:</h4>
                            <p>+1 5589 55488 55s</p>
                        </div>

                        </div>

                    </div>

                    <div class="col-lg-8 mt-5 mt-lg-0" data-aos="fade-left">

                        <form action="/send_post" method="post" role="form" class="php-email-form">
                        @csrf
                        <div class="form-row">
                            <div class="col-md-6 form-group">
                            <input type="text" name="name" class="form-control" id="name" placeholder="{{__('message.contact.name')}}" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                            <div class="validate"></div>
                            </div>
                            <div class="col-md-6 form-group">
                            <input type="tel" class="form-control" name="tel" id="tel" data-rule="required" placeholder="{{__('message.contact.tel')}}" pattern="+[0-9]{3,12}" data-msg="Please enter a valid phone number" />
                            <div class="validate"></div>
                            </div>
                        </div>  
                        <div class="form-group">
                            <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="{{__('message.contact.message')}}"></textarea>
                            <div class="validate"></div>
                        </div>
                        <div class="mb-3">
                            <div class="loading">Loading</div>
                            <!-- <div class="error-message"></div> -->
                            <!-- <div class="sent-message">Your message has been sent. Thank you!</div> -->
                        </div>
                        <div class="text-center"><button type="submit">Send Message</button></div>
                        </form>

                    </div>

                    </div>

                </div>
            </section><!-- End Contact Section -->

        </main><!-- End #main -->

        <!-- ======= Footer ======= -->
        <footer id="footer">
            <div class="container">
            <h3>Master Mix</h3>
            <p>{{__('message.footer.text')}}</p>
            <div class="social-links">
                <a href="#" class="twitter"><i class="bx bxl-telegram"></i></a>
                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                <a href="https://mail.google.com/mail/u/0/#inbox?compose=DmwnWtMpdKkPNCDHrMjgSxLQNkHvNrtvBvsPTJpGGBGnKwFVsfGkzxxcSKXKvTTHWlFpjBMGSTZQ" class="google-plus" target="_blank"><i class="icofont-email"></i></a>
            </div>
            <div class="copyright">
            &copy; {{__('message.footer.copyright')}} <strong><span>Master Mix</span></strong>. {{__('message.footer.copyright_2')}}
            </div>
            <div class="credits">
                <!-- All the links in the footer should remain intact. -->
                <!-- You can delete the links only if you purchased the pro version. -->
                <!-- Licensing information: https://bootstrapmade.com/license/ -->
                <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/selecao-bootstrap-template/ -->
                {{__('message.footer.design')}} <a href="https://t.me/deveprog7sh" target="_blank">DeveProg7Sh</a>
            </div>
            </div>
        </footer><!-- End Footer -->

        <a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
        <a href="#contact" class="back-to-contact"><i class="icofont-phone"></i></a>

        <!-- Vendor JS Files -->
        <script src="{{asset('assets/vendor/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('assets/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
        <script src="{{asset('assets/vendor/php-email-form/validate.js')}}"></script>
        <script src="{{asset('assets/vendor/isotope-layout/isotope.pkgd.min.js')}}"></script>
        <script src="{{asset('assets/vendor/venobox/venobox.min.js')}}"></script>
        <script src="{{asset('assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
        <script src="{{asset('assets/vendor/aos/aos.js')}}"></script>

        <!-- Template Main JS File -->
        <script src="{{asset('assets/js/main.js')}}"></script>

    </body>
</html>
