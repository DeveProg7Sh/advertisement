<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Mahsulot;
use App\Aloqa;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'remember_token' => Str::random(10),
    ];
});

$factory->define(Mahsulot::class, function (Faker $faker) {
    return [
        'nomi_uzb' => $faker->name,
        'nomi_rus' => $faker->name,
        'nomi_eng' => $faker->name,
        'link' => $faker->name,
        'sort' => $faker->name,
        'aktiv' => $faker->name,
        'hajm' => $faker->name,
        'gost' => $faker->name,
        'chakana' => $faker->name,
        'ulgurji' => $faker->name,
        'ulgurji_kg' => $faker->name,
        'chet_valyuta' => $faker->name,
        'qop' => $faker->name,
        'tavsif_uzb' => $faker->text(220),
        'tavsif_rus' => $faker->text(220),
        'tavsif_eng' => $faker->text(220),
        'tolov_uzb' => $faker->text(220),
        'tolov_rus' => $faker->text(220),
        'tolov_eng' => $faker->text(220),
        'yetkazish_uzb' => $faker->text(220),
        'yetkazish_rus' => $faker->text(220),
        'yetkazish_eng' => $faker->text(220),
        'deleted_at'=>''
    ];
});

$factory->define(Aloqa::class, function (Faker $faker) {
    return [
        'ism' => $faker->name,
        'telefon_raqam' =>$faker->regexify('09[0-9]{9}'),
        'xabar' => $faker->text,
        'holat' => "oqilmagan",
        'deleted_at' => ""
    ];
});