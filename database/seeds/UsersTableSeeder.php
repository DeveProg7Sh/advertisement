<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission_super_admin = [
            json_encode([
                "name"=>"Shaxsiy_Kabinet",
                "title"=>"Shaxsiy Kabinet",
                "url"=>"/",
                "iconUrl"=>"account_box",
                "read"=>true,
                "write"=>true,
                "update"=>true,
                "delete"=>true,
                "input"=>null,
                "output"=>null
            ]),
            json_encode([
                "name"=>"Mahsulot",
                "title"=>"Mahsulot",
                "url"=>"/mahsulot",
                "iconUrl"=>"inbox",
                "read"=>true,
                "write"=>true,
                "update"=>true,
                "delete"=>true,
                "input"=>null,
                "output"=>null
            ]),
            json_encode([
                "name"=>"Avtomobil",
                "title"=>"Avtomobil",
                "url"=>"/avtxizmat",
                "iconUrl"=>"inbox",
                "read"=>true,
                "write"=>true,
                "update"=>true,
                "delete"=>true,
                "input"=>null,
                "output"=>null
            ]),
            json_encode([
                "name"=>"Qandolat",
                "title"=>"Qandolat",
                "url"=>"/qandolat",
                "iconUrl"=>"add_shopping_cart",
                "read"=>true,
                "write"=>true,
                "update"=>true,
                "delete"=>true,
                "input"=>null,
                "output"=>null
            ]),
            json_encode([
                "name"=>"Xodim",
                "title"=>"Xodim",
                "url"=>"/xodim",
                "iconUrl"=>"manage_accounts",
                "read"=>true,
                "write"=>true,
                "update"=>true,
                "delete"=>true,
                "input"=>null,
                "output"=>null
            ]),
            json_encode([
                "name"=>"Foydalanuvchi",
                "title"=>"Foydalanuvchi",
                "url"=>"/users",
                "iconUrl"=>"person",
                "read"=>true,
                "write"=>true,
                "update"=>true,
                "delete"=>true,
                "input"=>null,
                "output"=>null
            ]),
            json_encode([
                "name"=>"Roles",
                "title"=>"Roles",
                "url"=>"/roles",
                "iconUrl"=>"settings",
                "read"=>true,
                "write"=>true,
                "update"=>true,
                "delete"=>true,
                "input"=>null,
                "output"=>null
            ])
        ];

        $permission_user = [
            json_encode([
                "name"=>"Shaxsiy_Kabinet",
                "title"=>"Shaxsiy Kabinet",
                "url"=>"/",
                "iconUrl"=>"account_box",
                "read"=>false,
                "write"=>false,
                "update"=>false,
                "delete"=>false,
                "input"=>null,
                "output"=>null
            ]),
            json_encode([
                "name"=>"Mahsulot",
                "title"=>"Mahsulot",
                "url"=>"/mahsulot",
                "iconUrl"=>"inbox",
                "read"=>false,
                "write"=>false,
                "update"=>false,
                "delete"=>false,
                "input"=>null,
                "output"=>null
            ]),
            json_encode([
                "name"=>"Foydalanuvchi",
                "title"=>"Foydalanuvchi",
                "url"=>"/users",
                "iconUrl"=>"person",
                "read"=>false,
                "write"=>false,
                "update"=>false,
                "delete"=>false,
                "input"=>null,
                "output"=>null
            ]),
            json_encode([
                "name"=>"Roles",
                "title"=>"Roles",
                "url"=>"/roles",
                "iconUrl"=>"settings",
                "read"=>false,
                "write"=>false,
                "update"=>false,
                "delete"=>false,
                "input"=>null,
                "output"=>null
            ])
        ];

        DB::table("roles")->insert(
            [
                "name" => "Super Admin",
                "permission" => "[" . implode(",", $permission_super_admin) . "]",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]   
        );
        DB::table("roles")->insert(
            [
                "name" => "User",
                "permission" => "[" . implode(",", $permission_user) . "]",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]   
        );
            
        DB::table("users")->insert(
            [
                "name" => "Admin",
                "email" => "admin@gmail.com ",
                "password" => bcrypt("miNda7@8$1"),
                "role_id" => "1",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]  
        );
        DB::table("users")->insert(
            [
                "name" => "User",
                "email" => "user@gmail.com ",
                "password" => bcrypt("12345678"),
                "role_id" => "2",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ]
        );

        
    }
}
