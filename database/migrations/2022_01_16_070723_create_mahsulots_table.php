<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMahsulotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahsulots', function (Blueprint $table) {
            $table->id();
            $table->string('nomi_uzb');
            $table->string('nomi_rus')->nullable();
            $table->string('nomi_eng')->nullable();
            $table->string('link')->nullable();
            $table->string('sort');
            $table->string('aktiv');
            $table->string('hajm');
            $table->string('gost');
            $table->string('chakana');
            $table->string('ulgurji');
            $table->string('ulgurji_kg');
            $table->string('chet_valyuta');
            $table->string('qop');
            $table->string('tavsif_uzb');
            $table->string('tavsif_rus')->nullable();
            $table->string('tavsif_eng')->nullable();
            $table->string('tolov_uzb');
            $table->string('tolov_rus')->nullable();
            $table->string('tolov_eng')->nullable();
            $table->string('yetkazish_uzb');
            $table->string('yetkazish_rus')->nullable();
            $table->string('yetkazish_eng')->nullable();
            $table->string('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahsulots');
    }
}
