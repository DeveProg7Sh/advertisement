<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateXodimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xodims', function (Blueprint $table) {
            $table->id();
            $table->string("ismi");
            $table->string("lavozimi");
            $table->string("telegram");
            $table->string("whatsapp");
            $table->string("epochta");
            $table->string("telnomer");
            $table->string("rasm")->nullable();
            $table->string('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xodims');
    }
}
