<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRasmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rasms', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mahsulot_id');
            $table->string('path');
            $table->timestamps();

            $table->foreign("mahsulot_id")->references("id")->on("mahsulots")->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rasms');
    }
}
