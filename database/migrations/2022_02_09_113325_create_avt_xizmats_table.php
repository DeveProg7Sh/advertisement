<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvtXizmatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avt_xizmats', function (Blueprint $table) {
            $table->id();
            $table->string("avtnomi");
            $table->string("hajm");
            $table->string("chakana");
            $table->string("ulgurji");
            $table->string("ulgurji_vaqt");
            $table->string("izoh_uzb");
            $table->string("izoh_rus")->nullable();
            $table->string("izoh_eng")->nullable();
            $table->string('tolov_uzb');
            $table->string('tolov_rus')->nullable();
            $table->string('tolov_eng')->nullable();
            $table->string('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avt_xizmats');
    }
}
