<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAvtXizmatRasmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avt_xizmat_rasms', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('avt_xizmat_id');
            $table->string('path');
            $table->timestamps();

            $table->foreign("avt_xizmat_id")->references("id")->on("avt_xizmats")->onDelete('cascade');         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avt_xizmat_rasms');
    }
}
