<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

use Illuminate\Http\Resources\Json\JsonResource;

class AvtXizmatCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "avtnomi" => $this->avtnomi,
            "hajm" => $this->hajm,
            'chakana' => $this->chakana,
            'ulgurji' => $this->ulgurji,
            'ulgurji_vaqt' => $this->ulgurji_vaqt,
            'izoh_uzb' => $this->izoh_uzb,
            'izoh_rus' => $this->izoh_rus,
            'izoh_eng' => $this->izoh_eng,
            'tolov_uzb' => $this->tolov_uzb,
            'tolov_rus' => $this->tolov_rus,
            'tolov_eng' => $this->tolov_eng,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'avtxizmatrasm' => $this->avtxizmatrasm
        ];
    }
}
