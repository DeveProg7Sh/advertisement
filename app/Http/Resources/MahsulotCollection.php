<?php

namespace App\Http\Resources;
use App\Mahsulot;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class MahsulotCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'nomi_uzb' => $this->nomi_uzb,
            'nomi_rus' => $this->nomi_rus,
            'nomi_eng' => $this->nomi_eng,
            'link' => $this->link,
            'sort' => $this->sort,
            'aktiv' => $this->aktiv,
            'hajm' => $this->hajm,
            'gost' => $this->gost,
            'chakana' => $this->chakana,
            'ulgurji' => $this->ulgurji,
            'ulgurji_kg' => $this->ulgurji_kg,
            'chet_valyuta' => $this->chet_valyuta,
            'qop' => $this->qop,
            'tavsif_uzb' => $this->tavsif_uzb,
            'tavsif_rus' => $this->tavsif_rus,
            'tavsif_eng' => $this->tavsif_eng,
            'tolov_uzb' => $this->tolov_uzb,
            'tolov_rus' => $this->tolov_rus,
            'tolov_eng' => $this->tolov_eng,
            'yetkazish_uzb' => $this->yetkazish_uzb,
            'yetkazish_rus' => $this->yetkazish_rus,
            'yetkazish_eng' => $this->yetkazish_eng,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'rasm' => $this->rasm
        ];
    }
}
