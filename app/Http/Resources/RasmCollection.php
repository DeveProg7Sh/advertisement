<?php

namespace App\Http\Resources;
use App\Rasm;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class RasmCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "mahsulot_id" => $this->mahsulot,
            "path" => $this->path
        ];
    }
}
