<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Resources\UserCollection;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class UsersController extends Controller
{
    public function AuthUser()
    {
        if (isset(Auth::user()->id)) {
            $user = User::where("id", Auth::user()->id)->get();

            return UserCollection::collection($user);
        }

        return response()->json([
            'msg'=> "You are not authorized"
        ], 401); 
        
    }

    public function Logout()
    {
        Auth::logout();

        return response()->json([
            'msg'=>'Success'
        ], 200);
    }

    public function GetUsers(Request $request)
    {
        $dt = User::orderBy("id")->where('role_id', '<>', 2)->get();

        return UserCollection::collection($dt);
    }

    public function CreateUser(Request $request)
    {
        if ($request->role_id == 1) {
            return response()->json([
                'msg'=> "The user cannot be assigned Super Admin role"
            ], 402);
        }
        $this->validate($request, [
            "name" => "required|min:3",
            "email" => "bail|required|email|unique:users",
            "password" => "bail|required|min:3",
            "role_id" => "required"
        ]);
        $data = [
            "name" => $request->name,
            "email" => $request->email,
            "role_id" => $request->role_id
        ];

        $password = $request->password;
        $password_repeat = $request->return_password;
        if($password != "" || $password_repeat != ""){
            if ($password != $password_repeat) {
                return response()->json([
                    'msg'=> "You entered two password differently"
                ], 422);        
            }
            $password = bcrypt($request->password);
            $data["password"] = $password;
        } 
        
        $user = User::create([
            "name" => $request->name,
            "email" => $request->email,
            "password" => $password,
            "role_id"  => $request->role_id,
        ]);

        return $user;
    }


    public function UpdateUser(Request $request)
    {
        if ($request->id == 1) {
            if ($request->role_id != 1) {
                return response()->json([
                    'msg'=> "This role of user is not update"
                ], 402);
            }
            $this->validate($request, [
                "id" => "required",
                "name" => "required|min:3",
                "email" => "bail|required|email|unique:users,email,$request->id",
                "role_id" => "required"
            ]);
            
            $data = [
                "name" => $request->name,
            ];
            $password = $request->password;
            $password_repeat = $request->return_password;
            if ($password != "" || $password_repeat != "") {
                if($password != $password_repeat){
                    return response()->json([
                        'msg'=> "You entered two password differently"
                    ], 422);        
                }
                $password = bcrypt($request->password);
                $data["password"] = $password;
            }        
            $user = User::where("id", $request->id)->update($data);

            return $user;
        }


        $this->validate($request, [
            "id" => "required",
            "name" => "required|min:3",
            "email" => "bail|required|email|unique:users,email,$request->id",
            "role_id" => "required"
        ]);
        
        $data = [
            "name" => $request->name,
            //"email" => $request->email, chunki emailni o`zgartirish uchun hali emailga kodlar jo`natish kerak
            "role_id"  => $request->role_id
        ];
        $p = $request->password;
        $p_r = $request->return_password;
        if($p != "" || $p_r != ""){
            if($p != $p_r){
                return response()->json([
                    'msg'=> "You entered two password differently"
                ], 422);        
            }
            $password = bcrypt($request->password);
            $data["password"] = $password;
        }        
        $user = User::where("id", $request->id)->update($data);
        return $user;
    }

    public function DeleteUser(Request $request)
    {
        if ($request->id == 1 && Auth::user()->id != $request->id) {
            return response()->json([
                'msg' => "This user is not delete"
            ], 402);
        }
        $this->validate($request, [
            "id" => "required",
            "email" => "bail|required|email|unique:users,email,$request->id",            
        ]);

        return User::where("email", $request->email)->delete();
    }

    public function DeleteUser1($request)
    {
        if ($request->id == 1 && Auth::user()->id != $request->id) {
            return response()->json([
                'msg'=> "This user is not delete"
            ], 402);
        }
        if (!isset($request->id)) 
            return "id not found";
        if (!isset($request->email)) 
            return "email not found";
        
        return User::where("email", $request->email)->delete();
    }

    public function MoreDeleteUser(Request $request)
    {
        $res = [];
        $i=0;
        while (isset($request[$i])) {
            $json = [
                'id' => $request[$i]['id'],
                'email' => $request[$i]['email']
            ];
            $res[] = $this->DeleteUser1(json_decode(json_encode($json)));
            $i++;
        }
        return response()->json([
            'data'=> $res
        ], 200); 
    }
}