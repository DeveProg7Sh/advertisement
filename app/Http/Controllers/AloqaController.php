<?php

namespace App\Http\Controllers;

use App\Aloqa;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class AloqaController extends Controller
{
    public function GetAloqa(Request $request)
    {
        return Aloqa::orderBy("id")->where('deleted_at', '=', '')->get();
    }

    public function ReadAloqa(Request $request)
    {
        $this->validate($request, [
            "id" => "required",
            "holat"=>"required"
        ]);     

        $data = [
            "holat"=>$request->holat
        ];
        $record_data = Aloqa::where("id", $request->id)->update($data);

        return $record_data;
    }

    public function DeleteAloqa(Request $request)
    {
        $this->validate($request, [
            "id"=>"required"           
        ]);
        $time = Carbon::now();
        $time = $time->ToDateTimeString();
        $data = [
            'deleted_at'=>$time
        ];
        $record_data = Aloqa::where("id", $request->id)->update($data);

        return $record_data;
        //return User::where("email", $request->email)->delete();
    }

    public function DeleteAloqa1($request)
    {
        if (!isset($request->id)) 
            return "id not found";
        $time = Carbon::now();
        $time = $time->ToDateTimeString();
        $data = [
            'deleted_at'=>$time
        ];

        return $record_data = Aloqa::where("id", $request->id)->update($data);
    }

    public function MoreDeleteAloqa(Request $request)
    {
        $res = [];
        $i=0;
        while (isset($request[$i])) {
            $json = [
                'id' => $request[$i]['id'],
            ];
            $res[] = $this->DeleteAloqa1(json_decode(json_encode($json)));
            $i++;
        }

        return response()->json([
            'data'=> $res
        ], 200); 
    }
}
