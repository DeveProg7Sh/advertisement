<?php

namespace App\Http\Controllers;
use App\AvtXizmat;
use App\Http\Resources\AvtXizmatCollection;
use App\AvtXizmatRasm;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;

class AvtXizmatController extends Controller
{
    public function GetAvtXizmat()
    {
        $data = AvtXizmat::orderBy("id")->where('deleted_at', '=', '')->get();
        
        return AvtXizmatCollection::collection($data);
    }

    public function CreateAvtXizmat(Request $request)
    {
        $this->validate($request, [
            'avtnomi' => "required|min:3",
            'hajm' => "required|min:1",
            'chakana' => "required|min:3",
            'ulgurji' => "required|min:3",
            'ulgurji_vaqt' => "required|min:1",
            'izoh_uzb' => "required|min:3",
            'izoh_rus' => "",
            'izoh_eng' => "",
            'tolov_uzb' => "required|min:3",
            'tolov_rus' => "",
            'tolov_eng' => ""
        ]);
        
        $record_data = AvtXizmat::create([
            'avtnomi' => $request->avtnomi,
            'hajm' => $request->hajm,
            'chakana' => $request->chakana,
            'ulgurji' => $request->ulgurji,
            'ulgurji_vaqt' => $request->ulgurji_vaqt,
            'izoh_uzb' => $request->izoh_uzb,
            'izoh_rus' => $request->izoh_rus,
            'izoh_eng' => $request->izoh_eng,
            'tolov_uzb' => $request->tolov_uzb,
            'tolov_rus' => $request->tolov_rus,
            'tolov_eng' => $request->tolov_eng,
            'deleted_at' => ''
        ]);

        return $record_data;
    }

    public function UpdateAvtXizmat(Request $request)
    {
        $this->validate($request, [
            'id' => "required",
            'avtnomi' => "required|min:3",
            'hajm' => "required|min:1",
            'chakana' => "required|min:3",
            'ulgurji' => "required|min:3",
            'ulgurji_vaqt' => "required|min:1",
            'izoh_uzb' => "required|min:3",
            'izoh_rus' => "",
            'izoh_eng' => "",
            'tolov_uzb' => "required|min:3",
            'tolov_rus' => "",
            'tolov_eng' => ""

        ]);     

        $data = [
            'avtnomi' => $request->avtnomi,
            'hajm' => $request->hajm,
            'chakana' => $request->chakana,
            'ulgurji' => $request->ulgurji,
            'ulgurji_vaqt' => $request->ulgurji_vaqt,
            'izoh_uzb' => $request->izoh_uzb,
            'izoh_rus' => $request->izoh_rus,
            'izoh_eng' => $request->izoh_eng,
            'tolov_uzb' => $request->tolov_uzb,
            'tolov_rus' => $request->tolov_rus,
            'tolov_eng' => $request->tolov_eng,
            'deleted_at' => ''
        ];
        $record_data = AvtXizmat::where("id", $request->id)->update($data);

        return $record_data;
    }

    public function DeleteAvtXizmat(Request $request)
    {
        $this->validate($request, [
            "id" => "required"           
        ]);
        $time = Carbon::now();
        $time = $time->ToDateTimeString();
        $data = [
            'deleted_at' => $time
        ];
        $record_data = AvtXizmat::where("id", $request->id)->update($data);

        return $record_data;
        //return User::where("email", $request->email)->delete();
    }

    public function DeleteAvtXizmat1($request)
    {
        if (!isset($request->id)) 
            return "id not found";
        $time = Carbon::now();
        $time = $time->ToDateTimeString();
        $data = [
            'deleted_at' => $time
        ];

        return $record_data = AvtXizmat::where("id", $request->id)->update($data);
    }

    public function MoreDeleteAvtXizmat(Request $request)
    {
        $res = [];
        $i=0;
        while (isset($request[$i])) {
            $json = [
                'id' => $request[$i]['id'],
            ];
            $res[] = $this->DeleteAvtXizmat1(json_decode(json_encode($json)));
            $i++;
        }

        return response()->json([
            'data' => $res
        ], 200); 
    }

    public function AvtXizmatRasmUpload(Request $request)
    {
        $this->validate($request, [
            "avt_xizmat_id" => "required",
            'file' => "required|mimes:jpeg,bmp,png,jpg"
        ]);
        $file_name = pathinfo($request->file->getClientOriginalName(), PATHINFO_FILENAME);
        $picName = $file_name . time() . "." . $request->file->extension();
        $request->file->move(storage_path("app/public"), $picName);
        $picName = "/storage/" . $picName;
        
        return  AvtXizmatRasm::create([
            "avt_xizmat_id" => $request->avt_xizmat_id,
            "path" => $picName
        ]);
    }

    public function AvtXizmatRasmDelete(Request $request)
    {
        $this->validate($request, [
            'file' => "required"
        ]);
        $filePath = str_replace("/storage/", "", $request->file);
        $filePath = storage_path("app/public/") . $filePath;
        if (file_exists($filePath)) {
            @unlink($filePath);
            AvtXizmatRasm::where("path", $request->file)->delete();
        } else {
            return response()->json([
                'data'=> "File Not Found"
            ], 402);
        }
    }
}
