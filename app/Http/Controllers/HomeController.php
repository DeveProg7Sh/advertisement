<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Aloqa;
use App\AvtXizmat;
use App\Mahsulot;
use App\Xodim;
class HomeController extends Controller
{
    public function Home()
    {
        $services = AvtXizmat::orderBy("id")->where('deleted_at', '=', '')->get();
        $products = Mahsulot::orderBy("id")->where('deleted_at', '=', '')->get();
        $xodimlar = Xodim::orderBy("id")->where('deleted_at', '=', '')->get();

        return view('welcome',['services'=>$services, 'products'=>$products, 'xodimlar'=>$xodimlar]);
    }

    public function Product($number)
    {
        $products = Mahsulot::orderBy("id")->where('deleted_at', '=', '')->get();
        $main_product = Mahsulot::orderBy("id")->where('deleted_at', '=', '')->where('id',$number)->first();
        
        return view('mahsulot',['products'=>$products,'main_product'=>$main_product]);
    }

    public function Service($number)
    {
        $products = AvtXizmat::orderBy("id")->where('deleted_at', '=', '')->get();
        $main_product = AvtXizmat::orderBy("id")->where('deleted_at', '=', '')->where('id',$number)->first();

        return view('avtxizmat',['services'=>$products,'main_product'=>$main_product]);
    }

    public function SendPost(Request $request)
    {
        $this->validate($request,[
            "name"=>"required|min:1",
            "tel"=>"required",
            "message"=>"required|max:4096|min:5"
        ]);
        $res = Session::get('applocale') == "uz" ? "Muvaffaqiyatli" : "";
        Aloqa::create([
            'ism'=>$request->name,
            'telefon_raqam'=>$request->tel,
            'xabar'=>$request->message,
            'holat'=>"oqilmagan",
            'deleted_at'=>''
        ]);
        if ($res == "") 
            $res = Session::get('applocale') == "ru" ? "Успешный" : "Successfully";

        return $res;
    }
}
