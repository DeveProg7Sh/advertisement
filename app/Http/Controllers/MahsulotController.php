<?php

namespace App\Http\Controllers;
use App\Mahsulot;
use App\Http\Resources\MahsulotCollection;
use App\Rasm;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;

class MahsulotController extends Controller
{
    public function GetMahsulot(Request $request)
    {
        $data = Mahsulot::orderBy("id")->where('deleted_at', '=', '')->get();

        return MahsulotCollection::collection($data);
    }

    public function CreateMahsulot(Request $request)
    {
        $this->validate($request, [
            'nomi_uzb' => "required|min:3",
            'nomi_rus' => "",
            'nomi_eng' => "",
            'link' => "",
            'sort' => "required|min:1",
            'aktiv' => "required|min:1",
            'hajm' => "required|min:1",
            'gost' => "required|min:3",
            'chakana' => "required|min:3",
            'ulgurji' => "required|min:3",
            'ulgurji_kg' => "required|min:3",
            'chet_valyuta' => "required|min:1",
            'qop' => "required|min:1",
            'tavsif_uzb' => "required|min:3",
            'tavsif_rus' => "",
            'tavsif_eng' => "",
            'tolov_uzb' => "required|min:3",
            'tolov_rus' => "",
            'tolov_eng' => "",
            'yetkazish_uzb' => "required|min:3",
            'yetkazish_rus' => "",
            'yetkazish_eng' => ""
        ]);
        
        $record_data = Mahsulot::create([
            'nomi_uzb' => $request->nomi_uzb,
            'nomi_rus' => $request->nomi_rus,
            'nomi_eng' => $request->nomi_eng,
            'link' => $request->link,
            'sort' => $request->sort,
            'aktiv' => $request->aktiv,
            'hajm' => $request->hajm,
            'gost' => $request->gost,
            'chakana' => $request->chakana,
            'ulgurji' => $request->ulgurji,
            'ulgurji_kg' => $request->ulgurji_kg,
            'chet_valyuta'=>$request->chet_valyuta,
            'qop' => $request->qop,
            'tavsif_uzb' => $request->tavsif_uzb,
            'tavsif_rus' => $request->tavsif_rus,
            'tavsif_eng' => $request->tavsif_eng,
            'tolov_uzb' => $request->tolov_uzb,
            'tolov_rus' => $request->tolov_rus,
            'tolov_eng' => $request->tolov_eng,
            'yetkazish_uzb' => $request->yetkazish_uzb,
            'yetkazish_rus' => $request->yetkazish_rus,
            'yetkazish_eng' => $request->yetkazish_eng,
            'deleted_at' => ''
        ]);

        return $record_data;
    }

    public function UpdateMahsulot(Request $request)
    {
        $this->validate($request, [
            "id" => "required",
            'nomi_uzb' => "required|min:3",
            'nomi_rus' => "",
            'nomi_eng' => "",
            'link' => "",
            'sort' => "required|min:1",
            'aktiv' => "required|min:1",
            'hajm' => "required|min:1",
            'gost' => "required|min:3",
            'chakana' => "required|min:3",
            'ulgurji' => "required|min:3",
            'ulgurji_kg' => "required|min:3",
            'chet_valyuta'=>"required|min:1",
            'qop' => "required|min:1",
            'tavsif_uzb' => "required|min:3",
            'tavsif_rus' => "",
            'tavsif_eng' => "",
            'tolov_uzb' => "required|min:3",
            'tolov_rus' => "",
            'tolov_eng' => "",
            'yetkazish_uzb' => "required|min:3",
            'yetkazish_rus' => "",
            'yetkazish_eng' => ""
        ]);     

        $data = [
            'nomi_uzb' => $request->nomi_uzb,
            'nomi_rus' => $request->nomi_rus,
            'nomi_eng' => $request->nomi_eng,
            'link' => $request->link,
            'sort' => $request->sort,
            'aktiv' => $request->aktiv,
            'hajm' => $request->hajm,
            'gost' => $request->gost,
            'chakana' => $request->chakana,
            'ulgurji' => $request->ulgurji,
            'ulgurji_kg' => $request->ulgurji_kg,
            'chet_valyuta'=>$request->chet_valyuta,
            'qop' => $request->qop,
            'tavsif_uzb' => $request->tavsif_uzb,
            'tavsif_rus' => $request->tavsif_rus,
            'tavsif_eng' => $request->tavsif_eng,
            'tolov_uzb' => $request->tolov_uzb,
            'tolov_rus' => $request->tolov_rus,
            'tolov_eng' => $request->tolov_eng,
            'yetkazish_uzb' => $request->yetkazish_uzb,
            'yetkazish_rus' => $request->yetkazish_rus,
            'yetkazish_eng' => $request->yetkazish_eng,
        ];
        $record_data = Mahsulot::where("id", $request->id)->update($data);

        return $record_data;
    }

    public function DeleteMahsulot(Request $request)
    {
        $this->validate($request, [
            "id" => "required"           
        ]);
        $time = Carbon::now();
        $time = $time->ToDateTimeString();
        $data = [
            'deleted_at' => $time
        ];
        $record_data = Mahsulot::where("id", $request->id)->update($data);

        return $record_data;
        //return User::where("email", $request->email)->delete();
    }

    public function DeleteMahsulot1($request)
    {
        if(!isset($request->id))
            return "id not found";
        $time = Carbon::now();
        $time = $time->ToDateTimeString();
        $data = [
            'deleted_at'=>$time
        ];

        return $record_data = Mahsulot::where("id", $request->id)->update($data);
    }

    public function MoreDeleteMahsulot(Request $request)
    {
        $res = [];
        $i=0;
        while (isset($request[$i])) {
            $json = [
                'id' => $request[$i]['id'],
            ];
            $res[] = $this->DeleteMahsulot1(json_decode(json_encode($json)));
            $i++;
        }

        return response()->json([
            'data'=> $res
        ], 200); 
    }

    public function RasmUpload(Request $request)
    {
        $this->validate($request, [
            "mahsulot_id" => "required",
            'file' => "required|mimes:jpeg,bmp,png,jpg"
        ]);
        $file_name =pathinfo($request->file->getClientOriginalName(), PATHINFO_FILENAME);
        $picName = $file_name . time() . "." . $request->file->extension();
        $request->file->move(storage_path("app/public"), $picName);
        $picName = "/storage/" . $picName;

        return  Rasm::create([
            "mahsulot_id"=>$request->mahsulot_id,
            "path"=>$picName
        ]);
    }

    public function RasmDelete(Request $request)
    {
        $this->validate($request, [
            'file' => "required"
        ]);
        $filePath = str_replace("/storage/", "", $request->file);
        $filePath = storage_path("app/public/") . $filePath;
        if (file_exists($filePath)) {
            @unlink($filePath);
            Rasm::where("path", $request->file)->delete();
        } else {
            return response()->json([
                'data'=> "File Not Found"
            ], 402);
        }
    }
}
