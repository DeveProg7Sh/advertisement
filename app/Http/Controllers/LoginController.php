<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\UserCollection;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            $user = User::where("id", Auth::user()->id)->get();
            if ($user[0]['role_id'] == 2) {
                return response()->json([
                        'msg'=> "You are not authorized"
                    ], 401);
            }
            $request->session()->regenerate();
            return UserCollection::collection($user);
        }

        return
            response()->json([
                'msg'=> "You are not authorized"
            ], 401);
    }
    
    public function logout(Request $request) 
    {
        $logout = Auth::logout();
        
        return $logout;
    }
}