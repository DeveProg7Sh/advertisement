<?php

namespace App\Http\Controllers;
use App\Xodim;
use App\Http\Resources\XodimCollection;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;

class XodimController extends Controller
{
    public function GetXodim(Request $request)
    {
        $data = Xodim::orderBy("id")->where('deleted_at', '=', '')->get();

        return XodimCollection::collection($data);
    }

    public function CreateXodim(Request $request)
    {
        $this->validate($request, [
            'ismi' => "required|min:3",
            'lavozimi' => "required|min:1",
            'telegram' => "required|min:1",
            'whatsapp' => "required|min:1",
            'epochta' => "required|min:3",
            'telnomer' => "required|min:3"
        ]);
        
        $record_data = Xodim::create([
            'ismi' => $request->ismi,
            'lavozimi' => $request->lavozimi,
            'telegram' => $request->telegram,
            'whatsapp' => $request->whatsapp,
            'epochta' => $request->epochta,
            'telnomer' => $request->telnomer,
            'deleted_at' => ''
        ]);

        return $record_data;
    }

    public function UpdateXodim(Request $request)
    {
        $this->validate($request, [
            'id' => "required|min:3",
            'ismi' => "required|min:3",
            'lavozimi' => "required|min:1",
            'telegram' => "required|min:1",
            'whatsapp' => "required|min:1",
            'epochta' => "required|min:3",
            'telnomer' => "required|min:3"
        ]);     

        $data = [
            'ismi' => $request->ismi,
            'lavozimi' => $request->lavozimi,
            'telegram' => $request->telegram,
            'whatsapp' => $request->whatsapp,
            'epochta' => $request->epochta,
            'telnomer' => $request->telnomer
        ];
        $record_data = Xodim::where("id", $request->id)->update($data);

        return $record_data;
    }

    public function DeleteXodim(Request $request)
    {
        $this->validate($request, [
            "id" => "required"           
        ]);
        $time = Carbon::now();
        $time = $time->ToDateTimeString();
        $data = [
            'deleted_at'=>$time
        ];
        $record_data = Xodim::where("id", $request->id)->update($data);

        return $record_data;
        //return User::where("email", $request->email)->delete();
    }

    public function DeleteXodim1($request)
    {
        if (!isset($request->id)) 
            return "id not found";
        $time = Carbon::now();
        $time = $time->ToDateTimeString();
        $data = [
            'deleted_at'=>$time
        ];

        return $record_data = Xodim::where("id", $request->id)->update($data);
    }

    public function MoreDeleteXodim(Request $request)
    {
        $res = [];
        $i=0;
        while (isset($request[$i])) {
            $json = [
                'id' => $request[$i]['id'],
            ];
            $res[] = $this->DeleteXodim1(json_decode(json_encode($json)));
            $i++;
        }

        return response()->json([
            'data' => $res
        ], 200); 
    }

    public function RasmUpload(Request $request)
    {
        $this->validate($request, [
            "id"=>"required",
            'file'=> "required|mimes:jpeg,bmp,png,jpg"
        ]);
        $file_name =pathinfo($request->file->getClientOriginalName(), PATHINFO_FILENAME);
        $picName = $file_name . time() . "." . $request->file->extension();
        $request->file->move(storage_path("app/public"), $picName);
        $picName = "/storage/" . $picName;

        $data = Xodim::where('id', $request->id)->first();
        if (!is_null($data->rasm)) {
            $filePath = str_replace("/storage/", "", $data->rasm);
            $filePath = storage_path("app/public/") . $filePath;
            if (file_exists($filePath)) {
                @unlink($filePath);
                Xodim::where("rasm", $data->rasm)->update(["rasm" => null]);
            }
        }
        Xodim::where("id", $request->id)->update(["rasm" => $picName]);

        return  response()->json([
            "data"=>$picName
        ], 200);
    }
}
