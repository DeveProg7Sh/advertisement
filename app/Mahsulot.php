<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahsulot extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'nomi_uzb', 'nomi_rus', 'nomi_eng', 'link', 'sort', 'aktiv', 'hajm', 'gost', 'chakana', 'ulgurji', 'ulgurji_kg', 'chet_valyuta', 'qop', 'tavsif_uzb', 'tavsif_rus', 'tavsif_eng', 'tolov_uzb', 'tolov_rus', 'tolov_eng', 'yetkazish_uzb', 'yetkazish_rus', 'yetkazish_eng', 'deleted_at', 'created_at', 'updated_at'];

    public function rasm()
    {
        return $this->hasMany('App\Rasm');
    }
}
