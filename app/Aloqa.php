<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aloqa extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'ism', 'telefon_raqam', 'xabar', 'holat', 'deleted_at', 'created_at', 'updated_at'];
}
