<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvtXizmat extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'avtnomi', 'hajm', 'chakana', 'ulgurji', 'ulgurji_vaqt', 'izoh_uzb', 'izoh_rus', 'izoh_eng', 'tolov_uzb', 'tolov_rus', 'tolov_eng', 'deleted_at', 'created_at', 'updated_at'];

    public function avtxizmatrasm()
    {
        return $this->hasMany("App\AvtXizmatRasm");
    }
}
