<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rasm extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'mahsulot_id', 'path'];

    public function mahsulot(){
        return $this->belongsTo('App\Mahsulot');
    }
}
