<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvtXizmatRasm extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'avt_xizmat_id', 'path', 'created_at', 'updated_at'];
}
