<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Xodim extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable=['id', "ismi", "lavozimi", "telegram", "whatsapp", "epochta", "telnomer", "rasm", "created_at", "updated_at", "deleted_at"];
}
