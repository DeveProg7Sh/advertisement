<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Asliddin
Route::post('/login', 'LoginController@authenticate');
//end Asliddin


//Sherozjon
Route::get('/logout', 'UsersController@Logout');
Route::prefix('/user')
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('/auth_user', 'UsersController@AuthUser');
        Route::get('/get_users', 'UsersController@GetUsers');
        Route::post('/create_user', 'UsersController@CreateUser');
        Route::post('/update_user', 'UsersController@UpdateUser');
        Route::post('/delete_user', 'UsersController@DeleteUser');
        Route::post('/more_delete_user', 'UsersController@MoreDeleteUser');
    });

Route::prefix('/role')
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('/get_roles', 'RolesController@GetRoles');
        Route::post('/create_role', 'RolesController@CreateRole');
        Route::post('/update_role', 'RolesController@UpdateRole');
        Route::post('/delete_role', 'RolesController@DeleteRole');
    });

Route::prefix('/mahsulot')
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('/get_mahsulot', 'MahsulotController@GetMahsulot');
        Route::post('/create_mahsulot', 'MahsulotController@CreateMahsulot');
        Route::post('/update_mahsulot', 'MahsulotController@UpdateMahsulot');
        Route::post('/delete_mahsulot', 'MahsulotController@DeleteMahsulot');
        Route::post('/more_delete_mahsulot', 'MahsulotController@MoreDeleteMahsulot');
        Route::post('/rasm_upload', 'MahsulotController@RasmUpload');
        Route::post('/rasm_delete', 'MahsulotController@RasmDelete');
    });
Route::prefix('/xodim')
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('/get_xodim', 'XodimController@GetXodim');
        Route::post('/create_xodim', 'XodimController@CreateXodim');
        Route::post('/update_xodim', 'XodimController@UpdateXodim');
        Route::post('/delete_xodim', 'XodimController@DeleteXodim');
        Route::post('/more_delete_xodim', 'XodimController@MoreDeleteXodim');
        Route::post('/rasm_upload', 'XodimController@RasmUpload');
    });
Route::prefix('/aloqa')
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('/get_aloqa', 'AloqaController@GetAloqa');
        Route::post('/read_aloqa', 'AloqaController@ReadAloqa');
        Route::post('/delete_aloqa', 'AloqaController@DeleteAloqa');
        Route::post('/more_delete_aloqa', 'AloqaController@MoreDeleteAloqa');
    });
Route::prefix('/avtxizmat')
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::get('/get_avtxizmat', 'AvtXizmatController@GetAvtXizmat');
        Route::post('/create_avtxizmat', 'AvtXizmatController@CreateAvtXizmat');
        Route::post('/update_avtxizmat', 'AvtXizmatController@UpdateAvtXizmat');
        Route::post('/delete_avtxizmat', 'AvtXizmatController@DeleteAvtXizmat');
        Route::post('/more_delete_avtxizmat', 'AvtXizmatController@MoreDeleteAvtXizmat');
        Route::post('/avtxizmatrasm_upload', 'AvtXizmatController@AvtXizmatRasmUpload');
        Route::post('/avtxizmatrasm_delete', 'AvtXizmatController@AvtXizmatRasmDelete');
    });
//end Sherozjon