<?php

use Illuminate\Support\Facades\Route;
use App\Http\Middleware\Language;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('lang/{lang}', 'LanguageController@swtichLang');

Route::get('/', function(){
    return redirect("/home");
})->middleware([Language::class]);

Route::get('/home', "HomeController@Home")->middleware([Language::class]);
Route::get('/product/{number}', "HomeController@Product")->middleware([Language::class]);
Route::get('/service/{number}', "HomeController@Service")->middleware([Language::class]);
Route::post('/send_post', 'HomeController@SendPost')->middleware([Language::class]);
// Route::get('home/{locale}', function ($locale) {
//     if (! in_array($locale, ['uz','ru','en'])) {
//         abort(400);
//     }

//     App::setLocale($locale);
//     return view('welcome');
// });


// Route::get('home/', function () {
//     App::setLocale('uz');
//     return view('welcome');
// });
