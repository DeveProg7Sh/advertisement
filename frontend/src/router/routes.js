const routes = [
  {
    path: '/login',
    component: () => import('layouts/LoginLayout.vue'),
  },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        name: 'Dashboard',
        component: () => import('pages/Index.vue'),
      },
      {
        path: '/mahsulot',
        name: 'Mahsulot',
        component: () => import('pages/mahsulot/mahsulot.vue'),
      },
      {
        path: '/avtxizmat',
        name: 'AvtXizmat',
        component: () => import('pages/avtxizmat/avtxizmat.vue'),
      },
      {
        path: '/xodim',
        name: 'Xodim',
        component: () => import('pages/xodim/xodim.vue'),
      },
      {
        path: '/qandolat',
        name: 'Qandolat',
        component: () => import('pages/qandolat/qandolat.vue'),
      },
      {
        path: '/users',
        name: 'Users',
        component: () => import('pages/users/users.vue'),
      },
      {
        path: '/roles',
        name: 'Roles',
        component: () => import('pages/roles/roles.vue'),
      }
    ],
  },
  {
    path: '/logout',
    component: () => import('layouts/LoginLayout.vue'),
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
]

export default routes