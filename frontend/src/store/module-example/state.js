export default function () {
  return {
    user: null,
    userPermission: null,
    stateResources: [
      {name: 'Dashboard',title: 'Dashboard', url: '/', iconUrl: "account_box", read:false, write: false, update: false, delete: false,input: null, output: null},
      {name: 'Mahsulot',title: 'Mahsulot', url: '/mahsulot', iconUrl: "inbox", read:false, write: false, update: false, delete: false,input: null, output: null},
      {name: 'Qandolat',title: 'Qandolat', url: '/qandolat', iconUrl: "add_shopping_cart", read:false, write: false, update: false, delete: false,input: null, output: null},
      {name: 'Xodim',title: 'Xodim', url: '/xodim', iconUrl: "manage_accounts", read:false, write: false, update: false, delete: false,input: null, output: null},
      {name: 'Users',title: 'Users', url: '/users', iconUrl: "person", read:false, write: false, update: false, delete: false,input: null, output: null},
      {name: 'Roles',title: 'Roles', url: '/roles', iconUrl: "settings", read:false, write: false, update: false, delete: false,input: null, output: null}
    ]
  }
  
}
